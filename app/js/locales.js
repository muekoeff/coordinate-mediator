var locales = {
	"de": {
		"button-done": "Fertig!",
		"continue-redirectto-pre": "Bitte beachte, dass du weitergeleitet wirst zu",
		"continue-redirectto-post": "",
		"dialog-close": "Schließe dieses Dialogfenster",
		"error-text": "Die verwendete URL ist unvollständig und enthält nicht alle benötigten Informationen, um fortfahren zu können.",
		"error-title": "Fehler",
		"introduction-text": "Bitte navigiere zum gewünschten Kartenausschnitt und klicke dann auf den grünen Pfeil am unteren Bildschirmrand."
	},
	"en": {
		"button-done": "I'm done",
		"continue-redirectto-pre": "Please note that you'll be redirected to",
		"continue-redirectto-post": "",
		"dialog-close": "Close this dialog window",
		"error-text": "The URL used is incomplete and does not contain all information needed in order to proceed.",
		"error-title": "Error",
		"introduction-text": "Please navigate to the desired map section and then click on the green arrow on the bottom of the screen."
	}
};