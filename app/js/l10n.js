function i10n(locale) {
	if(locales[locale] == "undefined") {
		console.warn(`No translation for ${locale}`);
		locale = "en";
	}

	document.querySelectorAll(`[data-l10n]`).forEach(element => {
		const message = locales[locale][element.getAttribute("data-l10n")]
		if(typeof message == "string") { 
			element.innerHTML = message; 
		}
	});
	document.querySelectorAll(`[data-l10n-title]`).forEach(element => {
		const message = locales[locale][element.getAttribute("data-l10n-title")]
		if(typeof message == "string") { 
			element.setAttribute("title", message);
		}
	});
}
