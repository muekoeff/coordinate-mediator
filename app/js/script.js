function main() {
	const params = loadParams();

	i10n(params.lang || "en");

	// Setup close-buttons in dialogs
	document.querySelectorAll("#panel-introduction .close").forEach(element => {
		element.addEventListener("click", function(e) {
			e.preventDefault();
			hide(element.closest(".visible"));
		});
	});

	// Close introduction after click anywhere
	window.addEventListener("click", function() {
		hide(document.getElementById("panel-introduction"));
		show(document.getElementById("panel-continue"));
	})

	// Load parameters
	if(params.valid) {
		const map = setupMap(params.lat, params.lon, params.zoom);
		setupUi(params, map);
		show(document.getElementById("panel-introduction"));
		setTimeout(function() {
			show(document.getElementById("panel-continue"));
		}, 250);
	} else {
		show(document.getElementById("panel-error"));
		console.warn("Missing or invalid parameters:", params);
		// @TODO
	}
}

function getTarget(targetTemplate, center, bbox, zoom) {
	return targetTemplate.replace(/{{centerlat}}/g, center.lat)
		.replace(/{{centerlon}}/g, center.lng)
		.replace(/{{east}}/g, bbox.getEast())
		.replace(/{{north}}/g, bbox.getNorth())
		.replace(/{{south}}/g, bbox.getSouth())
		.replace(/{{west}}/g, bbox.getWest())
		.replace(/{{zoom}}/g, zoom);
}

function loadParams() {
	const urlParams = new URLSearchParams(window.location.search);
	const target = urlParams.get("target");

	return {
		introduction: urlParams.get("introduction"),
		lang: urlParams.get("lang"),
		lat: urlParams.get("lat"),
		lon: urlParams.get("lon"),
		target: target,
		title: urlParams.get("title"),
		zoom: urlParams.get("zoom"),
		valid: target != null
	}
}

// https://stackoverflow.com/a/6878845
function getRandomInRange(from, to, fixed) {
    return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
}

function setupUi(params, map) {
	if(params.title != null) {
		document.getElementById("panel-introduction-title").textContent = params.title;
	}
	if(params.introduction != null) {
		document.getElementById("panel-introduction-text").textContent = params.introduction;
	}

	const targetUrl = new URL(params.target);
	document.querySelector("#panel-continue .param-host").textContent = targetUrl.host;

	document.querySelector("#continue-finish a").addEventListener("mouseover", function() {
		this.href = getTarget(params.target, map.getCenter(), map.getBounds(), map.getZoom())
	});
}

function setupMap(lat, lon, zoom) {
	const map = L.map("map", {
		attributionControl: false
	}).setView([lat || getRandomInRange(-85, 85, 3), lon || getRandomInRange(-175, 175, 3)], zoom || 5);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | <a href="http://muekev.de/imprint">Imprint</a>, <a href="https://gitlab.com/muekoeff/coordinate-mediator/">Source code</a>',
		maxZoom: 19
	}).addTo(map);
	L.control.scale().addTo(map);
	L.control.attribution({
		position: "topright"
	}).addTo(map);

	return map;
}

function hide(element) {
	element.classList.remove("visible");
	element.setAttribute("aria-hidden", "true");
}

function show(element) {
	element.classList.add("visible");
	element.setAttribute("aria-hidden", "false");
}

main();
