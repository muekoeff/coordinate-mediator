#!/bin/bash
mkdir public

cp -r app/* public/

# Leaflet
mkdir --parents public/lib/leaflet/
cp -r node_modules/leaflet/dist/* public/lib/leaflet/